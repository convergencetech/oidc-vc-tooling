const API_URL = process.env.API_URL || 'http://localhost:3002';
const EDDSA_ALG = 'EdDSA';
const { v4: uuid } = require('uuid');
const forge = require('node-forge');
const ED25519 = forge.pki.ed25519;
const bs58 = require('bs58');
const didKeyDriver = require('did-method-key').driver();
const axios = require('axios');
const base64url = require('base64url');

const KEY_ENDPOINT = 'https://api.dev.trybe.id/credentials/key';
const ISSUE_VC_ENDPOINT = 'https://api.dev.trybe.id/credentials/issue';
const PROVE_PRESENTATION_ENDPOINT = 'https://api.dev.trybe.id/presentations/prove';
const VERIFY_PRESENTATION_ENDPOINT = 'https://api.dev.trybe.id/presentations/verify';
const VERIFY_CREDENTIAL_ENDPOINT = 'https://api.dev.trybe.id/credentials/verify';

// Definition of the credential 
const credentialBase = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
    ],
    "id": `urn:uuid:${uuid()}`,
    "type": [
        "VerifiableCredential",
        "UniversityDegreeCredential"
    ],
    "issuer": {},
    "issuanceDate": "2020-03-10T04:24:12.164Z",
    "credentialSubject": {
        "degree": {
            "type": "BachelorDegree",
            "name": "Bachelor of Science and Arts"
        }
    }
};

const buildVerifiableCredential = async () => {
    const credential = Object.assign({}, credentialBase);

    // get new keys 
    const holderKey = await axios.get(KEY_ENDPOINT);
    const holderKeyId = holderKey.data.did;
    credential.credentialSubject.id = holderKeyId;

    const issuerKey = await axios.get(KEY_ENDPOINT);
    const issuerKeyId = issuerKey.data.did;
    credential.issuer.id = issuerKeyId;

    const vcResponse = await axios.post(ISSUE_VC_ENDPOINT, { credential });
    const verifiableCredential = vcResponse.data;

    return verifiableCredential;
}

const buildVerifiablePresentation = async ({ vc, options }) => {
    // Build presentation
    const presentation = {
        "@context": [
            "https://www.w3.org/2018/credentials/v1"
        ],
        "type": [ "VerifiablePresentation" ],
        "id": `urn:uuid:${uuid()}`,
        "holder": vc.credentialSubject.id,
        verifiableCredential: [ vc ]
    };

    // Get the verifiable presentation
    const vpResponse = await axios.post(PROVE_PRESENTATION_ENDPOINT, { presentation, options });
    const verifiablePresentation = vpResponse.data;

    return verifiablePresentation;
}

const buildIdToken = async ({ vp }) => {
    // Build the signed request
    const didDocument = await didKeyDriver.generate(); // ED25519 key type by default
    sub = didDocument.id;

    const sub_jwk = didDocument.publicKey[0];
    const { privateKeyBase58 } = didDocument.keys[sub_jwk.id];

    const privateKeyBase64 = bs58.decode(privateKeyBase58);
    const privateKey = Buffer.from(privateKeyBase64, 'base64');

    const payload = {
        'iss': 'https://wallet.example.com',
        sub,
        'aud': API_URL,
        'exp': (Date.now() / 1000) + 1000000,
        'iat': Date.now() / 1000,
        'nonce': uuid(),
        verifiablePresentations: [ vp ]
    };

    // ED25519 signing: https://tools.ietf.org/html/rfc8037#appendix-A.4
    const header = {
        alg: EDDSA_ALG,
        typ: 'JWT',
        kid: sub_jwk.id
    };

    const b64UrlHeader = base64url.fromBase64(
        Buffer.from(JSON.stringify(header), 'utf8').toString('base64')
    );
    const b64UrlPayload =  base64url.fromBase64(
        Buffer.from(JSON.stringify(payload), 'utf8').toString('base64')
    );

    const signature = ED25519.sign({
        // For V1.1.0+
        message: Buffer.from(`${b64UrlHeader}.${b64UrlPayload}`, 'utf8'),
        // message: Buffer.from(JSON.stringify(payload), 'utf8'),
        privateKey
    });

    // Built JWT
    const b64UrlSignature = base64url.fromBase64(signature.toString('base64'));
    const jwt = `${b64UrlHeader}.${b64UrlPayload}.${b64UrlSignature}`;

    return jwt;
}

const run = async () => {
    // Create the credential, signed by issuer
    const vc = await buildVerifiableCredential();
    console.log({ vc });

    // Verify the credential
    const vcVerifyResponse = await axios.post(VERIFY_CREDENTIAL_ENDPOINT, { verifiableCredential: vc });
    const vcVerification = vcVerifyResponse.data;
    console.log({ vcVerification });

    // Create the presentation, signed by holder (subject)
    const options = {
        challenge: uuid()
    };
    const vp = await buildVerifiablePresentation({ vc, options });
    console.log({ vp });

    // Verify the presentation
    const vpVerificationResponse = await axios.post(VERIFY_PRESENTATION_ENDPOINT, { verifiablePresentation: vp, options });
    const vpVerification = vpVerificationResponse.data;
    console.log({ vpVerification });

    // Build the id_token
    const id_token = await buildIdToken({ vp });
    console.log({ id_token });
}

run();
